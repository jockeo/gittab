import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabTopComponent } from './tab-top/tab-top.component';
import { TabDetailComponent } from './tab-show/tab-detail.component';

const routes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    { path: '', component:  TabTopComponent },
    { path: 'song/:id', component: TabDetailComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
