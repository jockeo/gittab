import * as _ from 'lodash';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SongChord } from 'src/models/songChord.model';
import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { RichSongChordService } from 'src/providers/rich-service/richSongChord.service';

@Component({
    selector: 'app-tab-top',
    templateUrl: './tab-top.component.html',
    styleUrls: ['./tab-top.component.scss']
})
export class TabTopComponent implements OnInit, OnDestroy {

    filteredList$: BehaviorSubject<SongChord[]> = new BehaviorSubject([]);
    currentFilter$: BehaviorSubject<string> = new BehaviorSubject('');
    filterValue: string;
    interactionSubscription: Subscription;

    constructor(protected songChordService: RichSongChordService, protected router: Router) { }

    ngOnInit() {
        this.interactionSubscription = combineLatest(this.currentFilter$, this.songChordService.getData())
            .subscribe(([filterValue, songChords]: [string, SongChord[]]) => {
                const filterValueLo = filterValue ? filterValue.toLocaleLowerCase() : null;
                const filteredSongChords = _(songChords).filter( songChord => {
                    const filterString = (songChord.artist.name + ' ' + songChord.songName).toLocaleLowerCase();
                    return filterValue ? filterString.indexOf(filterValueLo) > -1 : true;
                })
                .sortBy( songChord => (songChord.artist.name + ' ' + songChord.songName).toLocaleLowerCase()).value();
                this.filteredList$.next(filteredSongChords);
            });
    }

    ngOnDestroy(): void {
        this.interactionSubscription.unsubscribe();
    }

    updateFiltering(filterValue: string) {
        this.filterValue = filterValue;
        this.currentFilter$.next(filterValue);
    }

    selected(songchord: SongChord) {
        this.router.navigateByUrl('song/' + (songchord ? songchord.id : '0'));
    }

}
