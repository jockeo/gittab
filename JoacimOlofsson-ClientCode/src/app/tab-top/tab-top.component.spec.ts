import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabTopComponent } from './tab-top.component';

describe('TabTopComponent', () => {
  let component: TabTopComponent;
  let fixture: ComponentFixture<TabTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
