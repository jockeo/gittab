import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { SongChordService } from 'src/providers/data-service/songChord.service';
import { FooterComponent } from './footer/footer.component';
import { NavComponent } from './nav/nav.component';
import { MaterialModule } from './material.module';
import { TabTopComponent } from './tab-top/tab-top.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import { ArtistService } from 'src/providers/data-service/artist.service';
import { GenreService } from 'src/providers/data-service/genre.service';
import { TagService } from 'src/providers/data-service/tag.service';
import { UserService } from 'src/providers/data-service/user.service';
import { TabHtmlization } from 'src/providers/tabHtmlization.service';
import { TabDetailComponent } from './tab-show/tab-detail.component';
import { RichSongChordService } from 'src/providers/rich-service/richSongChord.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavComponent,
    TabTopComponent,
    TabDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MaterialModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
      SongChordService,
      ArtistService,
      GenreService,
      TagService,
      UserService,
      TabHtmlization,
      RichSongChordService,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
