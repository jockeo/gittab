import {
    MatAutocompleteModule,
    MatButtonModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatToolbarModule,
    MatSelectModule,
    MatCheckboxModule
} from '@angular/material';
import {MatCardModule} from '@angular/material/card';

import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        MatButtonModule,
        MatExpansionModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatCardModule,
        MatSliderModule,
        MatProgressBarModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatInputModule,
        MatCheckboxModule,
        MatGridListModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatListModule,
        MatDialogModule
    ],
    exports: [
        MatButtonModule,
        MatExpansionModule,
        MatMenuModule,
        MatIconModule,
        MatToolbarModule,
        MatCardModule,
        MatSelectModule,
        MatSliderModule,
        MatProgressBarModule,
        MatAutocompleteModule,
        MatInputModule,
        MatCheckboxModule,
        MatGridListModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatTooltipModule,
        MatListModule,
        MatDialogModule
    ],
})

export class MaterialModule {
}
