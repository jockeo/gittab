import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { TabHtmlization } from "src/providers/tabHtmlization.service";
import { SongChord } from "src/models/songChord.model";
import { RichSongChordService } from "src/providers/rich-service/richSongChord.service";
import { Observable, of, BehaviorSubject, Subscription, combineLatest } from "rxjs";
import { switchMap, map, filter, take } from "rxjs/operators";
import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { ArtistService } from "src/providers/data-service/artist.service";
import { GenreService } from "src/providers/data-service/genre.service";
import { TagService } from "src/providers/data-service/tag.service";
import { UserService } from "src/providers/data-service/user.service";
import { Artist } from "src/models/artist.model";
import { Genre } from "src/models/genre.model";
import { Tag } from "src/models/tag.model";
import { User } from "src/models/user.model";

@Component({
    selector: 'app-tab-detail',
    templateUrl: 'tab-detail.component.html',
    styleUrls: ['tab-detail.component.scss']
})

export class TabDetailComponent implements OnInit {

    Object: typeof Object = Object;

    songChord: SongChord;
    songSubject: BehaviorSubject<SongChord> = new BehaviorSubject(null);
    songObservable$ = this.songSubject.pipe(filter(res => res !== null));
    optionsObservable$: Observable<{ artist: { [id: number]: Artist }, genre: { [id: number]: Genre }, tag: { [id: number]: Tag }, user: { [id: number]: User } }>;
    songSubscriber: Subscription;
    toppings = new FormControl();
    editForm: FormGroup;
    songId: number;
    editMode: boolean = false;

    constructor(
        private artistService: ArtistService,
        private genreService: GenreService,
        private tagService: TagService,
        private userService: UserService,
        private activatedRoute: ActivatedRoute,
        private songChordService: RichSongChordService,
        private router: Router,
        public tabHtmlization: TabHtmlization,
        fb: FormBuilder
    ) { }

    ngOnInit(): void {
        this.optionsObservable$ =
            combineLatest(
                this.artistService.getDataById(),
                this.genreService.getDataById(),
                this.tagService.getDataById(),
                this.userService.getDataById())
                .pipe(map((res: any) => {
                    const item = {
                        artist: res[0],
                        genre: res[1],
                        tag: res[2],
                        user: res[3]
                    };
                    console.log(item);
                    return item;
                }));

        this.getSongObservable().pipe(take(1)).subscribe(songChord => {
            this.songSubject.next(songChord);
        });
    }

    getSongObservable(): Observable<SongChord> {
        return this.activatedRoute.params.pipe(
            switchMap((params: Params) => {
                if (params['id'] && +params['id'] !== 0) {
                    this.songId = +params['id'];
                    return this.songChordService.get(+params['id']).pipe(map(songChord => {
                        this.generateForm(songChord);
                        return songChord;
                    }));
                }
                return of(new SongChord()).pipe(map(songChord => {
                    this.songId = 0;
                    this.editMode = true;
                    this.generateForm(songChord);
                    return songChord;
                }));
            }));
    }

    generateForm(songChord: SongChord) {
        this.editForm = new FormGroup({
            artist: new FormControl(songChord.artist ? songChord.artist : ''),
            songName: new FormControl(songChord.songName ? songChord.songName : ''),
            genre: new FormControl(songChord.genre ? songChord.genre : ''),
            tags: new FormControl(songChord.tags ? songChord.tags : []),
            published: new FormControl(songChord.published),
            songData: new FormControl(songChord.songData ? songChord.songData : ''),
        });
    }

    updateTab() {
        this.songSubject.next(this.getFormAsSong());
    }

    getFormAsSong(): SongChord {
        const form = this.editForm.value;
        const songChord: SongChord = new SongChord();
        songChord.artist = form.artist;
        songChord.songName = form.songName;
        songChord.genre = form.genre;
        songChord.tags = form.tags;
        songChord.published = form.published;
        songChord.songData = form.songData;
        return songChord;
    }

    back() {
        this.router.navigateByUrl('/');
    }

    save() {
        if (this.songId === 0) {
            this.songChordService.post(this.getFormAsSong()).subscribe(() => this.editMode = false);
        } else {
            const song = this.getFormAsSong();
            song.id = this.songId;
            this.songChordService.put(this.songId, song).subscribe(() => this.editMode = false);
        }
    }

}
