import { Injectable } from "@angular/core";
import { SongChord, SongChordAPI } from "src/models/songChord.model";
import { ArtistService } from "../data-service/artist.service";
import { GenreService } from "../data-service/genre.service";
import { TagService } from "../data-service/tag.service";
import { UserService } from "../data-service/user.service";
import { BehaviorSubject, Observable, combineLatest } from "rxjs";
import { map, filter } from "rxjs/operators";
import { SongChordService } from "../data-service/songChord.service";
import * as _ from 'lodash';
import { Artist } from "src/models/artist.model";
import { Genre } from "src/models/genre.model";
import { User } from "src/models/user.model";
import { Tag } from "src/models/tag.model";

@Injectable()
export class RichSongChordService {

    data: BehaviorSubject<SongChord[]> = new BehaviorSubject(null);
    dataById: BehaviorSubject<{[id: number]: SongChord}> = new BehaviorSubject(null);

    fetchedAll: boolean = false;
    services: Array<any> = [];

    constructor(
        protected songChordService: SongChordService,
        protected artistService: ArtistService,
        protected genreService: GenreService,
        protected tagService: TagService,
        protected userService: UserService) {
            this.services = [
                songChordService.getData(),
                artistService.getDataById(),
                genreService.getDataById(),
                tagService.getDataById(),
                userService.getDataById()
            ];
    }

    getData(): Observable<SongChord[]> {
        if (!this.fetchedAll) {
            this.updateData();
            this.fetchedAll = true;
        }
        return this.data.pipe( filter( items => items != null) );
    }

    getDataById(): Observable<{[id: number]: SongChord}> {
        if (!this.fetchedAll) {
            this.updateData();
            this.fetchedAll = true;
        }
        return this.dataById.pipe( filter( items => items != null) );
    }

    get(id: number): Observable<SongChord> {
        return this.getDataById().pipe( map( items => items[id]));
    }

    // Posted item will surface through the base service and pushed to the cache dataset
    post(songChord: SongChord): Observable<void> {
        return this.songChordService.post(this.convertToApiModel(songChord)).pipe( map( res =>  null ));
    }

    put(id: number, songChord: SongChord): Observable<void> {
        return this.songChordService.put(id, this.convertToApiModel(songChord)).pipe( map( res =>  null ));
    }

    convertToApiModel(songChord: SongChord): SongChordAPI {
        const item = new SongChordAPI();
        item.id = songChord.id;
        item.artistId = songChord.artist ? songChord.artist.id : null;
        item.editedAt = new Date().toISOString();
        item.genreId = songChord.genre ? songChord.genre.id : null;
        item.published = songChord.published;
        item.songData = songChord.songData;
        item.songName = songChord.songName;
        item.tagIds = songChord.tags ? songChord.tags.map( tag => tag.id) : null;
        item.userId = songChord.user ? songChord.user.id : null;
        return item;
    }

    updateData() {
        combineLatest(this.services).pipe(
            map((res: [SongChordAPI[], {[id: number]: Artist}, {[id: number]: Genre}, {[id: number]: Tag}, {[id: number]: User}]) => {
                const allItems = this.mapData(res);
                this.data.next(allItems);
                this.dataById.next(_.keyBy(allItems, 'id'));
            })
        ).subscribe();
    }

    mapData([songChords, artist, genre, tag, user]: [SongChordAPI[], {[id: number]: Artist}, {[id: number]: Genre}, {[id: number]: Tag}, {[id: number]: User}]): SongChord[] {
        const mappedSongChords: SongChord[] = songChords.map( sourceObject => {
            const item = new SongChord();
            Object.assign(item, {
                id: sourceObject.id,
                songData: sourceObject.songData,
                songName: sourceObject.songName,
                createdAt: sourceObject.createdAt,
                editedAt: sourceObject.editedAt,
                published: sourceObject.published,
                artist: artist[sourceObject.artistId],
                genre: genre[sourceObject.genreId],
                tags: sourceObject.tagIds ? sourceObject.tagIds.map( id => tag[id]) : [],
                user: user[sourceObject.userId]
            });
            return item;
        });
        return mappedSongChords;
    }
}
