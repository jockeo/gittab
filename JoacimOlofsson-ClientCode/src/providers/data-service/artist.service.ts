import { Injectable } from "@angular/core";
import { BaseDataService } from "./baseData.service";
import { HttpClient } from "@angular/common/http";
import { Artist } from "src/models/artist.model";

@Injectable()
export class ArtistService extends BaseDataService<Artist> {

    constructor(http: HttpClient) {
        super("artist", http);
    }

}
