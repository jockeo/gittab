import { Injectable } from "@angular/core";
import { BaseDataService } from "./baseData.service";
import { HttpClient } from "@angular/common/http";
import { User } from "src/models/user.model";

@Injectable()
export class UserService extends BaseDataService<User> {

    constructor(http: HttpClient) {
        super("user", http);
    }

}
