import { Injectable } from "@angular/core";
import { BaseDataService } from "./baseData.service";
import { HttpClient } from "@angular/common/http";
import { SongChord, SongChordAPI } from "src/models/songChord.model";

@Injectable()
export class SongChordService extends BaseDataService<SongChordAPI> {

    constructor(http: HttpClient) {
        super("songchord", http);
    }

    mapData(data: SongChordAPI): SongChordAPI {
        const item = new SongChordAPI();
        Object.assign(item, data);
        return item;
    }

}
