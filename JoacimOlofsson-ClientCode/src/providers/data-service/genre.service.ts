import { Injectable } from "@angular/core";
import { BaseDataService } from "./baseData.service";
import { HttpClient } from "@angular/common/http";
import { Genre } from "src/models/genre.model";

@Injectable()
export class GenreService extends BaseDataService<Genre> {

    constructor(http: HttpClient) {
        super("genre", http);
    }

}
