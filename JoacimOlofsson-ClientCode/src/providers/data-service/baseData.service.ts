import { Observable, BehaviorSubject } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { map, filter, tap } from 'rxjs/operators';
import { BaseModel } from "src/models/baseData.model";
import { GenericHttpService } from "./genericHttp.service";
import * as _ from 'lodash';

export class BaseDataService<T extends BaseModel> {

    private data: BehaviorSubject<T[]> = new BehaviorSubject(null);
    private dataById: BehaviorSubject<{ [id: number]: T }> = new BehaviorSubject(null);

    protected httpService: GenericHttpService<T>;
    fetchedAll: boolean = false;

    constructor(protected path: string, protected http: HttpClient) {
        this.httpService = new GenericHttpService(path, http);
    }

    getData(): Observable<T[]> {
        if (this.fetchedAll === false) {
            this.updateAllData();
            this.fetchedAll = true;
        }
        return this.data.pipe(filter(res => res != null));
    }

    getDataById(): Observable<{ [id: number]: T }> {
        if (this.fetchedAll === false) {
            this.updateAllData();
            this.fetchedAll = true;
        }
        return this.dataById.pipe(filter(res => res != null));
    }

    updateAllData() {
        this.httpService.getAll().pipe(map(
            res => {
                this.data.next(res.map(item => this.mapData(item)));
                this.dataById.next(_.keyBy(res, 'id'));
            }
        )).subscribe();
    }

    get(id: number): Observable<T> {
        return this.httpService.get(id).pipe(
            map(res => {
                const items = this.data.value ? this.data.value : [];
                const item: T = this.mapData(res);
                const idx = _.findIndex(items, itm => itm.id === (+id));
                console.log(items);
                console.log(idx);
                if (idx > -1) {
                    items[idx] = item;
                } else {
                    items.push(item);
                }
                this.data.next(items);
                return item;
            })
        );
    }

    put(id: number, item: T): Observable<T> {
        return this.httpService.put(id, item).pipe(
            tap(res => {
                const items = this.data.value;
                const idx = items.map(itm => itm.id).indexOf(id);
                if (idx > -1) {
                    items[idx] = this.mapData(res);
                    this.data.next(items);
                }
            })
        );
    }

    post(item: T): Observable<T> {
        return this.httpService.post(item).pipe(
            tap(res => {
                const items = this.data.value;
                items.push(this.mapData(res));
                this.data.next(items);
            })
        );
    }

    delete(id: number): Observable<T> {
        return this.httpService.delete(id).pipe(
            tap(res => {
                const items = this.data.value;
                const idx = items.map(item => item.id).indexOf(id);
                this.data.next(items.splice(idx, 1));
            })
        );
    }

    mapData(data: T): T {
        return data;
    }

}
