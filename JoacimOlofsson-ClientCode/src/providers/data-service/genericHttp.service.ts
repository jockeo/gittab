import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

export class GenericHttpService<T> {

    basePath: string = "https://gittab.azurewebsites.net/api/";

    constructor(protected path, protected http: HttpClient) { }

    getAll(): Observable<T[]> {
        return this.http.get<T[]>(this.basePath + this.path);
    }

    get(id: number): Observable<T> {
        return this.http.get<T>(this.basePath + this.path + '/' + id);
    }

    put(id: number, item: T): Observable<T> {
        return this.http.put<T>(this.basePath + this.path + '/' + id, item);
    }

    post(item: T): Observable<T> {
        return this.http.post<T>(this.basePath + this.path, item);
    }

    delete(id: number): Observable<T> {
        return this.http.delete<T>(this.basePath + this.path + '/' + id);
    }

}
