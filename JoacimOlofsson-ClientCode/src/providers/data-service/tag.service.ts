import { Injectable } from "@angular/core";
import { BaseDataService } from "./baseData.service";
import { HttpClient } from "@angular/common/http";
import { Tag } from "src/models/tag.model";

@Injectable()
export class TagService extends BaseDataService<Tag> {

    constructor(http: HttpClient) {
        super("tag", http);
    }

}
