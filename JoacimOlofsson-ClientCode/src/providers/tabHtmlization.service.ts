import { Injectable } from '@angular/core';

@Injectable()
export class TabHtmlization {

    getSongHtmlized(trackdata): string {
        if (typeof trackdata === 'undefined') { return ''; }
        const inputText = trackdata;
        const preTag = '<pre>';
        const preEndTag = '</pre>';
        let chsong = '';
        let resultant = '';
        let inChord = false;
        let nChordTxt = 0;
        let skipChars = 0;
        for (let i = 0; i < inputText.length; i++) {
            const c = inputText[i];
            if (c === '(') { chsong += '<span class="chord">'; inChord = true; nChordTxt = 0; continue; }
            if (c === ')') { chsong += '</span>'; inChord = false; continue; }
            if (inChord === true) { chsong += c; skipChars++; nChordTxt++; continue; }
            if (c === '\n') { chsong += c; skipChars = 0; continue; }
            if (c === '{') { chsong += '<strong class="part">'; continue; }
            if (c === '}') { chsong += '</strong>'; continue; }
            if (c === '[') { continue; }
            if (c === ']') {
                resultant += preTag;
                resultant += chsong;
                resultant += preEndTag;
                chsong = '';
                continue;
            }
            chsong += c;
        }
        return resultant;
    }

}
