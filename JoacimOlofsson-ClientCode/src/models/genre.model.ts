import { BaseModel } from "./baseData.model";

export class Genre extends BaseModel {
    name: string;
}
