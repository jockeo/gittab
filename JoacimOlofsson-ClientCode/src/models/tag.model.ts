import { BaseModel } from "./baseData.model";

export class Tag extends BaseModel {
    name: string;
}
