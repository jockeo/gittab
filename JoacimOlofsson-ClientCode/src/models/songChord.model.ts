import { BaseModel } from "./baseData.model";
import { Artist } from "./artist.model";
import { Tag } from "./tag.model";
import { Genre } from "./genre.model";
import { User } from "./user.model";

export class SongChord {
    id: number;
    songData: string;
    songName: string;
    createdAt: string;
    editedAt: string;
    published: boolean;
    artist: Artist;
    tags: Tag[];
    genre: Genre;
    user: User;
}

export class SongChordAPI extends BaseModel {
    songData: string;
    songName: string;
    createdAt: string;
    editedAt: string;
    published: boolean;
    artistId: number;
    tagIds: number[];
    genreId: number;
    userId: number;
}
