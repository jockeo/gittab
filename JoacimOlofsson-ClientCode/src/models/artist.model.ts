import { BaseModel } from "./baseData.model";

export class Artist extends BaseModel {
    name: string;
}
