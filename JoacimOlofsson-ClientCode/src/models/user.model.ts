import { BaseModel } from "./baseData.model";

export class User extends BaseModel {
    email: string;
    password: string;
}
