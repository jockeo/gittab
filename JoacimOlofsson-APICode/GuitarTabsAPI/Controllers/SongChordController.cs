﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Business.BusinessModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class SongChordController : Controller
    {
        BusinessFacade bFacade;

        public SongChordController(BusinessFacade businessFacade)
        {
            bFacade = businessFacade;
        }

        [HttpGet]
        public ActionResult<List<SongChordBM>> Get()
        {
            return bFacade.SongChordService.GetAll();
        }

        [HttpGet("{id}", Name = "GetSongChord")]
        public ActionResult<SongChordBM> GetById(int id)
        {
            var item = bFacade.SongChordService.Get(id); ;
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Post([FromBody]SongChordBM item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var newItem = bFacade.SongChordService.Create(item);

            return CreatedAtRoute("GetGenre", new { id = newItem.Id }, newItem);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]SongChordBM item)
        {

            if (id != item.Id)
            {
                return BadRequest("Put path Id mismatch object Id.");
            }

            try
            {
                var updatedItem = bFacade.SongChordService.Update(item);
                return Ok(updatedItem);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(404, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = bFacade.SongChordService.Delete(id);
            if (item == null)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
