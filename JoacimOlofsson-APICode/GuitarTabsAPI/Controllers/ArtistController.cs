﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Business.BusinessModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class ArtistController : Controller
    {

        BusinessFacade bFacade;

        public ArtistController(BusinessFacade businessFacade) {
            bFacade = businessFacade;
        }

        [HttpGet]
        public ActionResult<List<ArtistBM>> Get()
        {
            return bFacade.ArtistService.GetAll();
        }

        [HttpGet("{id}", Name = "GetArtist")]
        public ActionResult<ArtistBM> GetById(int id)
        {
            var item = bFacade.ArtistService.Get(id); ;
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Post([FromBody]ArtistBM item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var newItem = bFacade.ArtistService.Create(item);

            return CreatedAtRoute("GetArtist", new { id = newItem.Id }, newItem);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]ArtistBM item)
        {

            if (id != item.Id)
            {
                return BadRequest("Put path Id mismatch object Id.");
            }

            try
            {
                var updatedArtist = bFacade.ArtistService.Update(item);
                return Ok(updatedArtist);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(404, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = bFacade.ArtistService.Delete(id);
            if (item == null)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
