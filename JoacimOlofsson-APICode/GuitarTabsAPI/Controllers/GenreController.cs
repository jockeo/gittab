﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Business.BusinessModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class GenreController : Controller
    {
        BusinessFacade bFacade;

        public GenreController(BusinessFacade businessFacade)
        {
            bFacade = businessFacade;
        }

        [HttpGet]
        public ActionResult<List<GenreBM>> Get()
        {
            return bFacade.GenreService.GetAll();
        }

        [HttpGet("{id}", Name = "GetGenre")]
        public ActionResult<GenreBM> GetById(int id)
        {
            var item = bFacade.GenreService.Get(id); ;
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Post([FromBody]GenreBM item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var newItem = bFacade.GenreService.Create(item);

            return CreatedAtRoute("GetGenre", new { id = newItem.Id }, newItem);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]GenreBM item)
        {

            if (id != item.Id)
            {
                return BadRequest("Put path Id mismatch object Id.");
            }

            try
            {
                var updatedGenre = bFacade.GenreService.Update(item);
                return Ok(updatedGenre);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(404, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = bFacade.GenreService.Delete(id);
            if (item == null)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
