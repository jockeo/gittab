﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Business.BusinessModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class TagController : Controller
    {
        BusinessFacade bFacade;

        public TagController(BusinessFacade businessFacade)
        {
            bFacade = businessFacade;
        }

        [HttpGet]
        public ActionResult<List<TagBM>> Get()
        {
            return bFacade.TagService.GetAll();
        }

        [HttpGet("{id}", Name = "GetTag")]
        public ActionResult<TagBM> GetById(int id)
        {
            var item = bFacade.TagService.Get(id); ;
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Post([FromBody]TagBM item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var newItem = bFacade.TagService.Create(item);

            return CreatedAtRoute("GetTag", new { id = newItem.Id }, newItem);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]TagBM item)
        {

            if (id != item.Id)
            {
                return BadRequest("Put path Id mismatch object Id.");
            }

            try
            {
                var updatedItem = bFacade.TagService.Update(item);
                return Ok(updatedItem);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(404, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = bFacade.TagService.Delete(id);
            if (item == null)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
