﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business;
using Business.BusinessModel;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        BusinessFacade bFacade;

        public UserController(BusinessFacade businessFacade)
        {
            bFacade = businessFacade;
        }

        [HttpGet]
        public ActionResult<List<UserBM>> Get()
        {
            return bFacade.UserService.GetAll();
        }

        [HttpGet("{id}", Name = "GetUser")]
        public ActionResult<UserBM> GetById(int id)
        {
            var item = bFacade.UserService.Get(id); ;
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult Post([FromBody]UserBM item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var newItem = bFacade.UserService.Create(item);

            return CreatedAtRoute("GetUser", new { id = newItem.Id }, newItem);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]UserBM item)
        {

            if (id != item.Id)
            {
                return BadRequest("Put path Id mismatch object Id.");
            }

            try
            {
                var updatedItem = bFacade.UserService.Update(item);
                return Ok(updatedItem);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(404, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = bFacade.UserService.Delete(id);
            if (item == null)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
