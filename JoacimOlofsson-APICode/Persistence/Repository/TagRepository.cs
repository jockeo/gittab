﻿using System;
using Microsoft.EntityFrameworkCore;
using Persistence.Entity;

namespace Persistence.Repository
{
    public class TagRepository : BaseRepository<Tag>
    {
        public TagRepository(DbSet<Tag> contextSet) : base(contextSet)
        {}
    }
}
