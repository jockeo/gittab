﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Entity;

namespace Persistence.Repository
{
    public class BaseRepository<T> : IGenericRepository<T> where T : BaseRepositoryModel
    {
        protected DbSet<T> _contextSet;

        public BaseRepository(DbSet<T> contextSet)
        {
            _contextSet = contextSet;
        }

        public virtual T Create(T item)
        {
            _contextSet.Add(item);
            return item;
        }

        public virtual T Delete(int id)
        {
            var item = Get(id);
            _contextSet.Remove(item);
            return item;
        }

        public virtual T Get(int id)
        {
            return _contextSet.FirstOrDefault(item => item.Id == id);
        }

        public virtual List<T> GetAll()
        {
            return _contextSet.ToList();
        }
    }
}
