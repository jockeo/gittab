﻿using System;
using Microsoft.EntityFrameworkCore;
using Persistence.Entity;

namespace Persistence.Repository
{
    public class ArtistRepository : BaseRepository<Artist>
    {
        public ArtistRepository(DbSet<Artist> contextSet) : base(contextSet)
        {}
    }
}
