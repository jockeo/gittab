﻿using System;
using Microsoft.EntityFrameworkCore;
using Persistence.Entity;

namespace Persistence.Repository
{
    public class GenreRepository : BaseRepository<Genre>
    {
        public GenreRepository(DbSet<Genre> contextSet) : base(contextSet)
        {}
    }
}
