﻿using System;
using Microsoft.EntityFrameworkCore;
using Persistence.Entity;

namespace Persistence.Repository
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(DbSet<User> contextSet) : base(contextSet)
        {}
    }
}
