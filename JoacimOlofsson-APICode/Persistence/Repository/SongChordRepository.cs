﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Persistence.Entity;

namespace Persistence.Repository
{
    public class SongChordRepository : BaseRepository<SongChord>
    {
        public SongChordRepository(DbSet<SongChord> contextSet) : base(contextSet)
        {}

        public override List<SongChord> GetAll()
        {
            return _contextSet.Include(sc => sc.Tags).ToList();
        }

        public override SongChord Get(int id)
        {
            return _contextSet.Include(sc => sc.Tags).FirstOrDefault(item => item.Id == id);
        }
    }
}
