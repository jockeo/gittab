﻿using System;
namespace Persistence.Entity
{
    public class SongChordTag
    {
        public int SongChordId { get; set; }
        public SongChord SongChord { get; set; }
        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
