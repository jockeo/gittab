﻿using System;
namespace Persistence.Entity
{
    public class Genre : BaseRepositoryModel
    {
        public string Name { get; set; }
    }
}
