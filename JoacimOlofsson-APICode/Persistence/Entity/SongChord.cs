﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence.Entity
{
    public class SongChord : BaseRepositoryModel
    {
        public string SongData { get; set; }
        public string SongName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime EditedAt { get; set; }
        public bool Published { get; set; }

        public int ArtistId { get; set; }
        public List<SongChordTag> Tags { get; set; }
        public int GenreId { get; set; }
        public int? UserId { get; set; }
    }
}
