﻿using System;
using System.Collections.Generic;

namespace Persistence.Entity
{
    public class Tag : BaseRepositoryModel
    {
        public string Name { get; set; }
        public List<SongChordTag> SongChords { get; set; }
    }
}
