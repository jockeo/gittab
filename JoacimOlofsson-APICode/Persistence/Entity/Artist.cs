﻿using System;
namespace Persistence.Entity
{
    public class Artist : BaseRepositoryModel
    {
        public string Name { get; set; }
    }
}
