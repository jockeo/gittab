﻿using System;
namespace Persistence.Entity
{
    public class User : BaseRepositoryModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
