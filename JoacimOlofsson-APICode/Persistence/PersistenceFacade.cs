﻿using System;
namespace Persistence
{
    public class PersistenceFacade
    {
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return new Persistence.UnitOfWork.UnitOfWork();
            }
        }


    }
}
