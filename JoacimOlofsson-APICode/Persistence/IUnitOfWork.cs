﻿using System;
using Persistence.Entity;

namespace Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Artist> ArtistRepository { get; }
        IGenericRepository<Genre> GenreRepository { get; }
        IGenericRepository<SongChord> SongChordRepository { get; }
        IGenericRepository<Tag> TagRepository { get; }
        IGenericRepository<User> UserRepository { get; }

        int Commit();

    }
}
