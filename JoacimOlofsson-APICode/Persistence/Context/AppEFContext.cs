﻿using System;
using Microsoft.EntityFrameworkCore;
using Persistence.Entity;

namespace Persistence.Context
{
    public class AppEFContext : DbContext
    {
        //static DbContextOptions<AppEFContext> options = new DbContextOptionsBuilder<AppEFContext>().UseInMemoryDatabase("MemoryDb").Options; 

        // public AppEFContext()
        //    : base(options)
        //{}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"...");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SongChordTag>()
                        .HasKey(sct => new { sct.SongChordId, sct.TagId });

            modelBuilder.Entity<SongChordTag>()
                        .HasOne(sct => sct.SongChord)
                        .WithMany(sc => sc.Tags)
                        .HasForeignKey(sct => sct.SongChordId);

            modelBuilder.Entity<SongChordTag>()
                        .HasOne(sct => sct.Tag)
                        .WithMany(t => t.SongChords)
                        .HasForeignKey(sct => sct.TagId);

            base.OnModelCreating(modelBuilder);
        } 

        public DbSet<Artist> Artists { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<SongChord> SongChords { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
