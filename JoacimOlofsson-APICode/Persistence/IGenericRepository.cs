﻿using System;
using System.Collections.Generic;

namespace Persistence
{
    public interface IGenericRepository<T>
    {
        T Create(T item);
        List<T> GetAll();
        T Get(int id);
        T Delete(int id);
    }
}
