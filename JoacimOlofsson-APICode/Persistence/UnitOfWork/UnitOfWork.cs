﻿using System;
using Persistence.Context;
using Persistence.Entity;
using Persistence.Repository;

namespace Persistence.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private AppEFContext context;

        public IGenericRepository<Artist> ArtistRepository { get; internal set; }
        public IGenericRepository<Genre> GenreRepository { get; internal set; }
        public IGenericRepository<SongChord> SongChordRepository { get; internal set; }
        public IGenericRepository<Tag> TagRepository { get; internal set; }
        public IGenericRepository<User> UserRepository { get; internal set; }

        public UnitOfWork()
        {
            context = new AppEFContext();
            context.Database.EnsureCreated();

            ArtistRepository = new ArtistRepository(context.Artists);
            GenreRepository = new GenreRepository(context.Genres);
            SongChordRepository = new SongChordRepository(context.SongChords);
            TagRepository = new TagRepository(context.Tags);
            UserRepository = new UserRepository(context.Users);
        }

        public int Commit()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
