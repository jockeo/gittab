﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Business.BusinessModel
{
    public class SongChordBM
    {
        public int Id { get; set; }
        public string SongData { get; set; }
        [Required]
        public string SongName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime EditedAt { get; set; }
        public bool Published { get; set; }

        public int ArtistId { get; set; }
        public List<int> TagIds { get; set; }
        public int GenreId { get; set; }
        public int? UserId { get; set; }
    }
}
