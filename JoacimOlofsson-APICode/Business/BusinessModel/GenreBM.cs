﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.BusinessModel
{
    public class GenreBM
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
