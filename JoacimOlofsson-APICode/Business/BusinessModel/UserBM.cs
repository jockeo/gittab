﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Business.BusinessModel
{
    public class UserBM
    {
        public int Id { get; set; }
        [Required]
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
