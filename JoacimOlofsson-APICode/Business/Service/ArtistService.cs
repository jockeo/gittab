﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.BusinessModel;
using Business.Converter;
using Persistence;

namespace Business.Service
{
    public class ArtistService : IGenericService<ArtistBM>
    {
        protected PersistenceFacade dbFacade;
        ArtistConverter converter = new ArtistConverter();

        public ArtistService(PersistenceFacade dbFacade)
        {
            this.dbFacade = dbFacade;
        }

        public ArtistBM Create(ArtistBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var newArtist = uow.ArtistRepository.Create(converter.Convert(Item));
                uow.Commit();
                return converter.Convert(newArtist);
            }
        }

        public ArtistBM Delete(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var deletedItem = uow.ArtistRepository.Delete(Id);
                uow.Commit();
                return converter.Convert(deletedItem);
            }
        }

        public ArtistBM Get(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return converter.Convert(uow.ArtistRepository.Get(Id));
            }
        }

        public List<ArtistBM> GetAll()
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return uow.ArtistRepository.GetAll()
                          .Select(converter.Convert)
                          .ToList();
            }
        }

        public ArtistBM Update(ArtistBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var itemFound = uow.ArtistRepository.Get(Item.Id);
                if (itemFound == null)
                {
                    throw new InvalidOperationException("Item not found");
                }
                itemFound.Name = Item.Name;
                uow.Commit();
                return converter.Convert(itemFound);
            }

        }

    }
}
