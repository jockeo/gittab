﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.BusinessModel;
using Business.Converter;
using Persistence;

namespace Business.Service
{
    public class UserService : IGenericService<UserBM>
    {
        protected PersistenceFacade dbFacade;
        UserConverter converter = new UserConverter();

        public UserService(PersistenceFacade dbFacade)
        {
            this.dbFacade = dbFacade;
        }

        public UserBM Create(UserBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var newUser = uow.UserRepository.Create(converter.Convert(Item));
                uow.Commit();
                return converter.Convert(newUser);
            }
        }

        public UserBM Delete(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var deletedItem = uow.UserRepository.Delete(Id);
                uow.Commit();
                return converter.Convert(deletedItem);
            }
        }

        public UserBM Get(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return converter.Convert(uow.UserRepository.Get(Id));
            }
        }

        public List<UserBM> GetAll()
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return uow.UserRepository.GetAll()
                          .Select(converter.Convert)
                          .ToList();
            }
        }

        public UserBM Update(UserBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var itemFound = uow.UserRepository.Get(Item.Id);
                if (itemFound == null)
                {
                    throw new InvalidOperationException("Item not found");
                }
                itemFound.Email = Item.Email;
                itemFound.Password = Item.Password;
                uow.Commit();
                return converter.Convert(itemFound);
            }

        }
    }
}
