﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.BusinessModel;
using Business.Converter;
using Persistence;

namespace Business.Service
{
    public class TagService : IGenericService<TagBM>
    {
        protected PersistenceFacade dbFacade;
        TagConverter converter = new TagConverter();

        public TagService(PersistenceFacade dbFacade)
        {
            this.dbFacade = dbFacade;
        }

        public TagBM Create(TagBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var newTag = uow.TagRepository.Create(converter.Convert(Item));
                uow.Commit();
                return converter.Convert(newTag);
            }
        }

        public TagBM Delete(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var deletedItem = uow.TagRepository.Delete(Id);
                uow.Commit();
                return converter.Convert(deletedItem);
            }
        }

        public TagBM Get(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return converter.Convert(uow.TagRepository.Get(Id));
            }
        }

        public List<TagBM> GetAll()
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return uow.TagRepository.GetAll()
                          .Select(converter.Convert)
                          .ToList();
            }
        }

        public TagBM Update(TagBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var itemFound = uow.TagRepository.Get(Item.Id);
                if (itemFound == null)
                {
                    throw new InvalidOperationException("Item not found");
                }
                itemFound.Name = Item.Name;
                uow.Commit();
                return converter.Convert(itemFound);
            }

        }
    }
}
