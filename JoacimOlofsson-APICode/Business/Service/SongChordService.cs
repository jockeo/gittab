﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.BusinessModel;
using Business.Converter;
using Persistence;

namespace Business.Service
{
    public class SongChordService : IGenericService<SongChordBM>
    {
        protected PersistenceFacade dbFacade;
        SongChordConverter converter = new SongChordConverter();

        public SongChordService(PersistenceFacade dbFacade)
        {
            this.dbFacade = dbFacade;
        }

        public SongChordBM Create(SongChordBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var newItem = uow.SongChordRepository.Create(converter.Convert(Item));
                uow.Commit();
                return converter.Convert(newItem);
            }
        }

        public SongChordBM Delete(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var deletedItem = uow.SongChordRepository.Delete(Id);
                uow.Commit();
                return converter.Convert(deletedItem);
            }
        }

        public SongChordBM Get(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return converter.Convert(uow.SongChordRepository.Get(Id));
            }
        }

        public List<SongChordBM> GetAll()
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return uow.SongChordRepository.GetAll()
                          .Select(converter.Convert)
                          .ToList();
            }
        }

        public SongChordBM Update(SongChordBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var itemFound = uow.SongChordRepository.Get(Item.Id);
                if (itemFound == null)
                {
                    throw new InvalidOperationException("Item not found");
                }
                var itemUpdated = converter.Convert(Item);
                itemFound.SongData = Item.SongData;
                itemFound.SongName = Item.SongName;
                itemFound.CreatedAt = Item.CreatedAt;
                itemFound.EditedAt = DateTime.Now;
                itemFound.Published = Item.Published;

                itemFound.ArtistId = Item.ArtistId;
                itemFound.GenreId = Item.GenreId;
                itemFound.UserId = Item.UserId;
                itemFound.Tags.RemoveAll(
                    sct => !itemUpdated.Tags.Exists(
                        t => t.SongChordId == sct.SongChordId &&
                        t.TagId == sct.TagId
                ));

                itemUpdated.Tags.RemoveAll(
                    sct => itemFound.Tags.Exists(
                        t => t.TagId == sct.TagId &&
                        t.SongChordId == sct.SongChordId
                    ));

                itemFound.Tags.AddRange(itemUpdated.Tags);

                uow.Commit();
                return converter.Convert(itemFound);
            }

        }
    }
}
