﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.BusinessModel;
using Business.Converter;
using Persistence;

namespace Business.Service
{
    public class GenreService : IGenericService<GenreBM>
    {
        protected PersistenceFacade dbFacade;
        GenreConverter converter = new GenreConverter();

        public GenreService(PersistenceFacade dbFacade)
        {
            this.dbFacade = dbFacade;
        }

        public GenreBM Create(GenreBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var newGenre = uow.GenreRepository.Create(converter.Convert(Item));
                uow.Commit();
                return converter.Convert(newGenre);
            }
        }

        public GenreBM Delete(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var deletedItem = uow.GenreRepository.Delete(Id);
                uow.Commit();
                return converter.Convert(deletedItem);
            }
        }

        public GenreBM Get(int Id)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return converter.Convert(uow.GenreRepository.Get(Id));
            }
        }

        public List<GenreBM> GetAll()
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                return uow.GenreRepository.GetAll()
                          .Select(converter.Convert)
                          .ToList();
            }
        }

        public GenreBM Update(GenreBM Item)
        {
            using (var uow = dbFacade.UnitOfWork)
            {
                var itemFound = uow.GenreRepository.Get(Item.Id);
                if (itemFound == null)
                {
                    throw new InvalidOperationException("Item not found");
                }
                itemFound.Name = Item.Name;
                uow.Commit();
                return converter.Convert(itemFound);
            }

        }

    }
}
