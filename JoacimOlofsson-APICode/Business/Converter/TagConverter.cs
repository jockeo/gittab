﻿using System;
using Business.BusinessModel;
using Persistence.Entity;

namespace Business.Converter
{
    public class TagConverter
    {
        internal Tag Convert(TagBM itemBM)
        {
            if (itemBM == null) return null;
            return new Tag()
            {
                Id = itemBM.Id,
                Name = itemBM.Name,
            };
        }

        internal TagBM Convert(Tag item)
        {
            if (item == null) return null;
            return new TagBM()
            {
                Id = item.Id,
                Name = item.Name,
            };
        }
    }
}
