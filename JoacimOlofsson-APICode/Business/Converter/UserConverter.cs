﻿using System;
using Business.BusinessModel;
using Persistence.Entity;

namespace Business.Converter
{
    public class UserConverter
    {
        internal User Convert(UserBM itemBM)
        {
            if (itemBM == null) return null;
            return new User()
            {
                Id = itemBM.Id,
                Email = itemBM.Email,
                Password = itemBM.Password
            };
        }

        internal UserBM Convert(User item)
        {
            if (item == null) return null;
            return new UserBM()
            {
                Id = item.Id,
                Email = item.Email,
                //Password = item.Password
            };
        }
    }
}
