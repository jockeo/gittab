﻿using System;
using System.Linq;
using Business.BusinessModel;
using Persistence.Entity;

namespace Business.Converter
{
    public class SongChordConverter
    {
        internal SongChord Convert(SongChordBM itemBM)
        {
            if (itemBM == null) return null;
            return new SongChord()
            {
                Id = itemBM.Id,
                SongData = itemBM.SongData,
                SongName = itemBM.SongName,
                CreatedAt = itemBM.CreatedAt,
                EditedAt = itemBM.EditedAt,
                Published = itemBM.Published,

                ArtistId = itemBM.ArtistId,
                GenreId = itemBM.GenreId,
                UserId = itemBM.UserId,
                Tags = itemBM.TagIds?.Select(tId => new SongChordTag()
                {
                    TagId = tId,
                    SongChordId = itemBM.Id
                }).ToList()
            };
        }

        internal SongChordBM Convert(SongChord item)
        {
            if (item == null) return null;
            return new SongChordBM()
            {
                Id = item.Id,
                SongData = item.SongData,
                SongName = item.SongName,
                CreatedAt = item.CreatedAt,
                EditedAt = item.EditedAt,
                Published = item.Published,

                ArtistId = item.ArtistId,
                TagIds = item.Tags?.Select( t => t.TagId).ToList(),
                GenreId = item.GenreId,
                UserId = item.UserId,
            };
        }
    }
}
