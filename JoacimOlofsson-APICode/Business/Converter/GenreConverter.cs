﻿using System;
using Business.BusinessModel;
using Persistence.Entity;

namespace Business.Converter
{
    public class GenreConverter
    {
        internal Genre Convert(GenreBM itemBM)
        {
            if (itemBM == null) return null;
            return new Genre()
            {
                Id = itemBM.Id,
                Name = itemBM.Name,
            };
        }

        internal GenreBM Convert(Genre item)
        {
            if (item == null) return null;
            return new GenreBM()
            {
                Id = item.Id,
                Name = item.Name,
            };
        }
    }
}
