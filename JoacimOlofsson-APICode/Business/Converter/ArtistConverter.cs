﻿using System;
using Business.BusinessModel;
using Persistence.Entity;

namespace Business.Converter
{
    public class ArtistConverter
    {
        internal Artist Convert(ArtistBM itemBM)
        {
            if (itemBM == null) return null;
            return new Artist()
            {
                Id = itemBM.Id,
                Name = itemBM.Name,
            };
        }

        internal ArtistBM Convert(Artist item)
        {
            if (item == null) return null;
            return new ArtistBM()
            {
                Id = item.Id,
                Name = item.Name,
            };
        }
    }
}
