﻿using System;
using Business.BusinessModel;
using Business.Service;
using Persistence;

namespace Business
{
    public class BusinessFacade
    {
        public IGenericService<ArtistBM> ArtistService
        {
            get { return new ArtistService(new PersistenceFacade()); }
        }

        public IGenericService<GenreBM> GenreService
        {
            get { return new GenreService(new PersistenceFacade()); }
        }

        public IGenericService<SongChordBM> SongChordService
        {
            get { return new SongChordService(new PersistenceFacade()); }
        }

        public IGenericService<TagBM> TagService
        {
            get { return new TagService(new PersistenceFacade()); }
        }

        public IGenericService<UserBM> UserService
        {
            get { return new UserService(new PersistenceFacade()); }
        }
    }
}
