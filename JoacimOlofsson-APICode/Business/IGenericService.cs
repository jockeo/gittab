﻿using System;
using System.Collections.Generic;

namespace Business
{
    public interface IGenericService<T>
    {
        T Create(T Item);
        List<T> GetAll();
        T Get(int Id);
        T Update(T Item);
        T Delete(int Id);
    }
}
